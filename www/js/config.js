/********APP WORKS HOURS****************/
/*localhost*/
//var HOST_URL = 'http://localhost/UTE-DEV/';
/* localhost - DEV */
var HOST_URL = 'https://www.efene.xyz/cashapp/';
/* *******************API URL************************ */
var PROD_URL = 'API/index.php/';
var DEV_URL = 'API/index.php/';
var DEV_MODE = 1;
var BASE_URL = HOST_URL + ( DEV_MODE ? DEV_URL : PROD_URL);
/* *************************************************** */

/* ********************DEVICES*********************** */
var DEVICE = navigator.userAgent;

/*API RESPONSES*/
var  RESPONSE_SUCCESS = 1;
var RESPONSE_FAILURE = 0;
var FN_WORKING = -1;

var ADMIN_PANEL_URL = HOST_URL + 'cashapp/';

/* FORMS */
var LIST = 4;
var ADD = 1;
var EDIT = 2;
var VIEW = 3;

/*COMPANY*/
var COMPANY = 1;

/* TABLES */
var USERS_TABLE = 1;
var HOURS_TABLE = 2;
var TEXTS_TABLE = 3;
var EMPLOYEES_TABLE = 4;
var PROJECTS_TABLE = 5;

/* COMPANY */
var VEINTICINCO_DE_MAYO = "1";
var DOCE_DE_OCTUBRE = "2";
var EL_LIBERTADOR = "3";
var PERALTA_RAMOS = "4";
var COSTA_AZUL = "5";
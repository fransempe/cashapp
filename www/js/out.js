function fillOutItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['OUT'];
    var dataBindings =
    {
        '|FECHA|': itemDataContent['FECHA'],
        '|IMPORTE|': itemDataContent['IMPORTE'],
        '|DESCRIPTION|': itemDataContent['DESCRIPTION'],
        '|COMPANY|': nameCompany( itemDataContent['COMPANY_ID'] )
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllOut ( OutId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_Out/getAllOut",
        "Out_id="+OutId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function addOut(fecha, importe, description, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_out_add/addOut",
        "&fecha_egreso="+fecha+"&importe="+importe+"&descripcion="+description,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllOut()
{
    alert("Not data loaded in the data base!");
}

function addOutCallback( labelId )
{
    dataLoadedSuccessfully( labelId, 'Egreso agregado con éxito.' );
    removeAtInputs();
}

function failAddOut()
{
    alert("Error al ingresar el Egreso.");
}

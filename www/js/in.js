function fillInItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['IN'];
    var dataBindings =
    {
        '|FECHA|': itemDataContent['FECHA'],
        '|IMPORTE|': itemDataContent['IMPORTE'],
        '|DESCRIPTION|': itemDataContent['DESCRIPTION'],
        '|COMPANY|': nameCompany( itemDataContent['COMPANY_ID'] )
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllIn ( InId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_In/getAllIn",
        "In_id="+InId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function addIn(fecha, importe, description, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_in_add/addIn",
        "&fecha_ingreso="+fecha+"&importe="+importe+"&descripcion="+description,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllIn()
{
    alert("Not data loaded in the data base!");
}

function addInCallback( labelId )
{
    dataLoadedSuccessfully( labelId, 'Ingreso agregado con éxito.' );
    removeAtInputs();
}

function failAddIn()
{
    alert("Error al ingresar el Ingreso.");
}

function fillFuelConsumedItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['FUEL_CONSUMED_DATA'];
    var dataBindings =
    {
        '|FECHA|': itemDataContent['FECHA'],
        '|IMPORTE|': itemDataContent['IMPORTE'],
        '|LITROS|': itemDataContent['LITROS'],
        '|DESCRIPTION|': itemDataContent['DESCRIPTION'],
        '|TIPO_TANQUE|': itemDataContent['TIPO_TANQUE'],
        '|INTERNO|': itemDataContent['INTERNO'],
        '|DOMINIO|': itemDataContent['DOMINIO'],
        '|TIPO|': itemDataContent['TIPO'],
        '|COMPANY|': nameCompany( itemDataContent['COMPANY_ID'] )
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function getAllFuelConsumed ( fuelConsumedId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_fuel_consumed/getFuelConsumed",
        "fuelConsumed_id="+fuelConsumedId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function addFuelConsumed(fecha, precio, litrosCargados, kms, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_fuel_consumed/addFuelConsumed",
        "&fecha="+fecha+"&precio="+precio+"&litrosCargados="+litrosCargados+"&kms="+kms,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetAllFuelConsumed()
{
    alert("Not data loaded in the data base!");
}

function addFuelTankCallback( labelId )
{
    dataLoadedSuccessfully( labelId, 'Tanque agregado con éxito.' );
    removeAtInputs();
}

function failAddFuelTank()
{
    alert("NO");
}

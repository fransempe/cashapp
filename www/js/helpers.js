function loadPage(url, targetContainer)
{
    loadPageWithParameters(url, targetContainer, []);
}

function loadPageWithParameters(url, targetContainer, parameters)
{
    $.ajax({
            url: url,
            async: false,
            success: function (data)
            {
                for (var key in parameters)
                    data = replaceAll(data, key, parameters[key]);

                $(targetContainer).append(data);
            },
            dataType: 'html'
        });
}

function escapeRegExp(string)
{
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace)
{
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceDataBindings(dataBindings, template)
{
    for (var key in dataBindings)
        template = replaceAll(template, key, dataBindings[key]);
    return template;
}

function showElement(elementId)
{
    document.getElementById(elementId).style.display = 'block';
}

function closeElement(elementId)
{
    document.getElementById(elementId).style.display = 'none';
}

function showSectionContent(sectionFile)
{
    document.getElementById("content").innerHTML = "";
    loadPage(sectionFile, "#content");
}

function closeAllElement()
{
    closeElement("hoursTableTemplate");
    closeElement("employeesTableTemplate");
    closeElement("usersTableTemplate");
    closeElement("projectsTableTemplate");
}

function closeTablet(elementId)
{
    footerPosition(2);
    document.getElementById(elementId).style.display = 'none';
}

function footerPosition(position)
{
    if (position==1)
    {
        document.getElementById('footerAdminPanel').style.position = 'relative';
    }
    if (position==2)
    {
        document.getElementById('footerAdminPanel').style.position = 'absolute';
    }
}

function formatParameters(parameters)
{
    var params = '';
    for (var key in parameters)
    {
        var value = parameters[key];
        if (value)
            params += key + '=' + encodeURI(parameters[key]) + '&';
    }
    if (params)
        return params.substring(0, params.length - 1);
    return '';
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

function fillListWithTemplate(data, itemTemplate, fillHandler)
{
    return fillListWithTemplateWithKey('content', data, itemTemplate, fillHandler);
}

function fillListWithTemplateWithKey(jsonKey, data, itemTemplate, fillHandler)
{
    var itemList = "";

    for (var i = 0; i < (data[jsonKey].length); i++)
    {
        data[jsonKey][i]['position'] = i;
        itemList += fillHandler(data[jsonKey][i], itemTemplate);
    }
    return itemList;
}

function getTemplate(templateName)
{
    var templateHtml = '';
    $.ajax(
        {
            url: './templates/' + templateName + '.html',
            async: false,
            success: function (data)
            {
                templateHtml = data;
            },
            dataType: 'html'
        }
    );
    return templateHtml;
}

function subStrings(text)
{
    var response = text;
    if (text.length > 19)
    {
        response = response.substring(0, 25) + '...';
    }
    return(response);
}

function validate(evt)
{
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}

function locationVars(elementRequired)
{
    var src = String( window.location.href ).split('?')[1];
    var vrs = src.split('&');

    for (var x = 0, c = vrs.length; x < c; x++)
    {
        if (vrs[x].indexOf(elementRequired) != -1)
        {
            return decodeURI( vrs[x].split('=')[1] );
            break;
        };
    };
};

function soloNumeros(e)
{
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}

function isNumberKey(evt) 
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    } 
    else
    {
        if (evt.target.value.search(/\./) > -1 && charCode == 46) {
            return false;
        }
        return true;
    }
}

function nameCompany( companyId )
{
    switch(companyId)
            {
                case VEINTICINCO_DE_MAYO:
                      return('25 de Mayo');
                break;
                case DOCE_DE_OCTUBRE:
                       return('12 de Octubre');
                break;
                case EL_LIBERTADOR:
                    return('El Libertador');
                break;
                case PERALTA_RAMOS:
                    return('Peralta Ramos');
                break;
                case COSTA_AZUL:
                    return('Costa Azul');
                break;
            }
}

function emptyInputText(id)
{
    document.getElementById(id).value = '';
}

function emptyLabelText(id)
{
    document.getElementById(id).innerHTML = '';
}

function dataEntryNull(id, text)
{
    document.getElementById(id).style.color = '#006ebd';
    document.getElementById(id).innerHTML = text;
}

function fieldEmpty(id, text)
{
    document.getElementById(id).style.color = '#e80000';
    document.getElementById(id).innerHTML = text;
}

function dataEntryOk(id, text)
{
    document.getElementById(id).style.color = '#c8c8c8';
    document.getElementById(id).innerHTML = text;
}

function dataLoadedSuccessfully(id, text)
{
    document.getElementById(id).style.color = '#3A9A3A';
    document.getElementById(id).innerHTML = text;
    showElement(id);
}


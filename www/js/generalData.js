function fillTodayItem ( itemData, itemTemplate )
{
    var dataBindings =
    {
        '|TODAY|': itemData['TODAY']
    };

    return replaceDataBindings(dataBindings, itemTemplate);
}

function getToday( successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_today/getToday",
        "",
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function failGetToday()
{
    alert("Not data loaded in the data base!");
}

<?php
class encrypt
{
    const ENCRYPT_YES = "1";
    const ENCRYPT_NO = "0";
}


function encrypt( $pass, $cryptKey )
{
    $passEncoded = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $pass, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
    return( $passEncoded );
}

function decrypt( $pass, $cryptKey )
{
    $passDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $pass ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
    return( $passDecoded );
}


?>
<?php
//Paths for routes.
define('APPPATH', 'application/');

//Include conection file and configs files
include_once('conection/company.php');
include_once('conection/data_base.php');
include_once('conection/connect.php');
include_once('responses/errors.php');
include_once('json/JsonPretty.php');
include_once('helpers/helpers.php');

//Security files
include_once('security/access.php');

//Sql files
include_once('sql/sql.php');
include_once('sql/login.php');
include_once('sql/hardware.php');
include_once('sql/identification.php');
include_once('sql/adds.php');
include_once('sql/edits.php');
include_once('sql/deletes.php');
include_once('sql/search.php');
include_once('sql/testing.php');

//Sent emails files
include_once('eMails/sentEmails.php');

global $AMOUNT_QUERY_RESULTS;
$AMOUNT_QUERY_RESULTS = 0;
global $contentJson;
$contentJson = array();

class config
{
    //hardware identification by call type
        const IDENTIFICATION_HARDWARE_LOGIN = 1;
        const IDENTIFICATION_HARDWARE_CALL = 2;

    //APPS
        const APP_WEB_BROWSER = "1";
        const APP_IOS = "2";
        const APP_ANDROID = "3";
        const APP_WINDOWS_PHONE = "4";
        const APP_DESCKTOP = "5";

    //Conection Config
        const HOST = "localhost";
        const API_PATH = "/cashapp/www/API/index.php/";

    /****Data Base parameters*****/
        const DATA_BASE_USER = "root";

        const DATA_BASE_USER_PASS ="";

    //Developers Names
        const DEVELOPER_NAME = "FN-Software Factory: ";

}
?>
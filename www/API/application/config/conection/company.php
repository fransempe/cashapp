<?php
class company
{
    const ALL_COMPANY = 0;
    const VEINTICINCO_DE_MAYO = 1;
    const DOCE_OCTUBRE = 2;
    const EL_LIBERTADOR = 3;
    const PERALTA_RAMOS = 4;
    const COSTA_AZUL = 5;
    const UTE = 6;
    const CARAPP = 7;
    const AMOUNT_COMPANY = 8;

    const UTE_DETAIL = 'UTE';
    const DOCE_OCTUBRE_DETAIL = '12 de Octubre';
    const EL_LIBERTADOR_DETAIL = 'El Libertador';
    const VEINTICINCO_DE_MAYO_DETAIL = '25 de Mayo';
    const PERALTA_RAMOS_DETAIL = 'Peralta Ramos';
    const COSTA_AZUL_DETAIL = 'Costa Azul';
    const CARAPP_ALL_VEHICLES = 'CARAPP FOR ALL VEHICLES';
}
?>
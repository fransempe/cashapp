<?php

class JsonPretty
{
    public function prettify($json)
    {
        if (!$this->isJson($json)) {
            return $this->process(json_encode($json));
        }
        return $this->process($json);
    }
    protected function process($json, $indent = "\t")
    {
        $result = '';
        $indentCount = 0;
        $inString = false;
        $len = strlen($json);
        for ($c = 0; $c < $len; $c++) {
            $char = $json[$c];
            if ($char === '{' || $char === '[') {
                if (!$inString) {
                    $indentCount++;
                    if ($char === '[' && $json[$c+1]  == "]") {
                        $result .= $char . PHP_EOL;
                    } elseif ($char === '{' && $json[$c+1]  == "}") {
                        $result .= $char . PHP_EOL;
                    } else {
                        $result .= $char . PHP_EOL . str_repeat($indent, $indentCount);
                    }
                } else {
                    $result .= $char;
                }
            } elseif ($char === '}' || $char === ']') {
                if (!$inString) {
                    $indentCount--;
                    $result .= PHP_EOL . str_repeat($indent, $indentCount) . $char;
                } else {
                    $result .= $char;
                }
            } elseif ($char === ',') {
                if (!$inString) {
                    $result .= ',' . PHP_EOL . str_repeat($indent, $indentCount);
                } else {
                    $result .= $char;
                }
            } elseif ($char === ':') {
                if (!$inString) {
                    $result .= ': ';
                } else {
                    $result .= $char;
                }
            } elseif ($char === '"') {
                if (
                    // A String is ending, when there is a not escaped quote ...
                    ($c > 0 && $json[$c - 1] !== '\\')
                    &&
                    // and a String is ending, when there is a quote prepended with a escaped slash.
                    ($c > 1 && $json[$c - 2].$json[$c - 1] === '\\\\')
                ) {
                    $inString = !$inString;
                }
                $result .= $char;
            } else {
                $result .= $char;
            }
        }
        return $result."\n";
    }
    protected function isJson($string)
    {
        if (!is_string($string)) {
            return false;
        }
        json_decode($string);
        return json_last_error() == JSON_ERROR_NONE;
    }

    function printJson()
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;
        $testingUsers {'success'} = 1;
        $testingUsers {'result_found'} = $AMOUNT_QUERY_RESULTS;
        $jsonPretty = new JsonPretty;
        echo $jsonPretty->prettify( $testingUsers + $contentJson );
    }
}

?>
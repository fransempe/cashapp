<?php
include_once(dirname(__FILE__).'./../config/config.php');
class app_in_add
{
    public function addIn()
    {
        $COMPANY = company::ALL_COMPANY;

        if (empty($_GET['fecha_ingreso']))
        {
            isRequired( 'fecha_ingreso' );
        }
        if (empty($_GET['importe']))
        {
            isRequired( 'importe' );
        }
        if (empty($_GET['descripcion']))
        {
            isRequired( 'descripcion' );
        }

        if ( isset($_GET['fecha_ingreso']) && isset($_GET['importe']) && isset($_GET['descripcion']) )
        {
            $fecha_ingreso = $_GET['fecha_ingreso'];
            $importe = $_GET['importe'];
            $descripcion = $_GET['descripcion'];
            $QUERY_TABLE = 'queryAddin';

            $PARAMETERS = array( $fecha_ingreso, $importe, $descripcion );
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_in_add', 'addInJson', $PARAMETERS );
        }
    }

    function addInJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {

        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'In, Loaded Successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        }
    }
}
?>
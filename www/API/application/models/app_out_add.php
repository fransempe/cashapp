<?php
include_once(dirname(__FILE__).'./../config/config.php');
class app_out_add
{
    public function addOut()
    {
        $COMPANY = company::ALL_COMPANY;

        if (empty($_GET['fecha_egreso']))
        {
            isRequired( 'fecha_egreso' );
        }
        if (empty($_GET['importe']))
        {
            isRequired( 'importe' );
        }
        if (empty($_GET['descripcion']))
        {
            isRequired( 'descripcion' );
        }

        if ( isset($_GET['fecha_egreso']) && isset($_GET['importe']) && isset($_GET['descripcion']) )
        {
            $fecha_egreso = $_GET['fecha_egreso'];
            $importe = $_GET['importe'];
            $descripcion = $_GET['descripcion'];
            $QUERY_TABLE = 'queryAddout';

            $PARAMETERS = array( $fecha_egreso, $importe, $descripcion );
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_out_add', 'addOutJson', $PARAMETERS );
        }
    }

    function addOutJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {

        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Out Loaded Successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        }
    }
}
?>
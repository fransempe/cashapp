<head>
<script>
    function getDataLogin()
    {
        var rememberMe = document.getElementById('rememberMe').checked;
        if (rememberMe == true)
        {
            REMEMBER_LOGIN = 1;
        }
        var userName =  document.getElementById('userName').value;
        var userPass =  document.getElementById('userPass').value;
        userLogin(userName, userPass, loginCallback, incorrectLogin, workingDesignPopupLoginFNCallback);
    }

    function workingDesignPopupLoginFNCallback()
    {
        showElement("divIframeViewLogin");
    }

    function incorrectLogin()
    {
        showElement('loginIncorrect');
    }

</script>
</head>

<body>
<div class="ch-container" id="loginContent" style="">
                <div class="row">
                    <div class="col-md-12 center login-header" style="background-image: url(); background-repeat: no-repeat; background-position: center;">
                    </div>
                    <h4 style="text-align: center;">Bienvenido a CashAPP®</h4>
                    <!--/span-->
                </div><!--/row-->
                <div class="row">
                    <div class="well col-md-5 center login-box">
                        <div class="alert alert-info">
                            Please enter your username and password.
                        </div>
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user blue"></i></span>
                                    <input onkeydown="" id="userName" type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="clearfix"></div><br>

                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock blue"></i></span>
                                    <input onkeydown="" id="userPass" type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="clearfix"></div>

                                <div class="input-prepend">
                                    <label class="remember" for="remember"><input type="checkbox" id="rememberMe"> Remember me.</label>
                                </div>
                                <div id="loginIncorrect" class="clearfix" style="">User or pass incorrect.</div>
                            </fieldset>
                        </form>
                        <p class="center col-md-5">
                            <button onclick="getDataLogin();" class="btn btn-primary">Login</button>
                        </p>
                    </div><!--/span-->
                </div><!--/row-->
</div><!--/.fluid-container-->
</html>

<?php
include_once(dirname(__FILE__).'./../config.php');

function sentLogEmail ()
{
    //Variables de datos de ERROR
    @$empresa = 'QM - Equipment';
    @$name = 'Estacion de trabajo donde ocurrio';
    @$location = 'Mar del Plata - Argentina';
    @$message = 'Informacion del error encontrado';
    $head = "From: QM Equipment\n" //La persona que envia el correo
        . "Reply-To: QM - Equipment\n";
    $cco = "DETECTED - LOG"; //asunto aparecera en la bandeja del servidor de correo
    $email_to = "nicolas.corleto@fnsystems.com.ar"; //your e-mail.
    $content = "$empresa ha enviado un mensaje desde QM - Equipment / Sistema de horas.\n"
        . "\n"
        . "Maquina: $name\n"
        . "Localidad: $location\n"
        . "Mensaje: $message\n"
        . "\n";
    @mail($email_to, $cco ,$content ,$head );
}
?>
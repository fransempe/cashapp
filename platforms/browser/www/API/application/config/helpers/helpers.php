<?php
include_once(dirname(__FILE__).'./../config.php');

/******Json vars**********************/
global $AMOUNT_QUERY_RESULTS;
$AMOUNT_QUERY_RESULTS = 0;
global $contentJson;
$contentJson = array();

function remove_tildes( $string ) {
    $not_allowed= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $alloweed= array    ("a","e","i","o","u","A","E","I","O","U","n","N","A","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $text = str_replace($not_allowed, $alloweed ,$string);
    return $text;
}

function setCompanyDetail( $COMPANY_DETAIL )
{
	switch ( $COMPANY_DETAIL ) 
	{
        case company::UTE:
            $COMPANY_DETAIL = company::UTE_DETAIL;
        break;
		case company::DOCE_OCTUBRE:
			$COMPANY_DETAIL = company::DOCE_OCTUBRE_DETAIL;
		break;
		case company::EL_LIBERTADOR:
			$COMPANY_DETAIL = company::EL_LIBERTADOR_DETAIL;
		break;
		case company::VEINTICINCO_DE_MAYO:
			$COMPANY_DETAIL = company::VEINTICINCO_DE_MAYO_DETAIL;
		break;
		case company::PERALTA_RAMOS:
			$COMPANY_DETAIL = company::PERALTA_RAMOS_DETAIL;
		break;
		case company::COSTA_AZUL:
			$COMPANY_DETAIL = company::COSTA_AZUL_DETAIL;
		break;
	}
	return ( $COMPANY_DETAIL );
}

?>
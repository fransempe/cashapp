<?php

include_once(dirname(__FILE__).'./../config.php');

function queryFuelConsumedGeneralSearch( $PARAMETERS )
{
    $fuelConsumedGeneral_id = $PARAMETERS[0];

    $sql = "SELECT T.ID, T.ID_EMPRESA, T.TOTAL_LTS, T.FECHA, T.DELETED, M.empresa_id, M.razon_social, M.cuit
            FROM fuel_consume T
            WHERE T.ID_EMPRESA = M.empresa_id
            AND T.DELETED = 'N'";

    if ( $fuelConsumedGeneral_id != '' )
    {
        $sql = $sql." AND T.ID = $fuelConsumedGeneral_id ";
    }

    $sql = $sql." ORDER BY  T.FECHA DESC";
    $table = mysql_query( $sql );
    setDataTableError( $table );
    return ( $table );
}

?>
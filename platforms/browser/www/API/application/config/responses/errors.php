<?php
include_once(dirname(__FILE__).'./../config.php');

function loginSetDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
    if( mysql_num_rows($table) == 0 )
        setError( 'LOGIN INCORRECT', 0 );
}

function setDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
    /*I commented this code for not data loaded in the data base.*/
    //if( mysql_num_rows($table) == 0 )
       // setError( 'NO DATA LOADED INTO THE DATA BASE', 0 );
}

function addDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
}

function editDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
}

function isRequired( $data )
{
    setError($data.' is Required!', 0);
}

function userNotFound( $data )
{
    setError($data.' User not found!', 0);
}

function setError( $message, $success )
{
    $jsonPretty = new JsonPretty;
    echo $jsonPretty->prettify (array( 'success' => $success, 'data' => '', 'message' => $message, 'developer name' => config::DEVELOPER_NAME ));
    exit;
}

?>
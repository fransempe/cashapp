<?php
include_once(dirname(__FILE__).'./../config/config.php');
class app_fuel_consume
{

    public function addFuelConsume()
    {
        $COMPANY = company::ALL_COMPANY;

        if (empty($_GET['date_consume']))
        {
            isRequired( 'date_consume' );
        }
        if (empty($_GET['totalLts']))
        {
            isRequired( 'totalLts' );
        }
        if (empty($_GET['totalCost']))
        {
            isRequired( 'totalCost' );
        }
        if (empty($_GET['kms']))
        {
            isRequired( 'kms' );
        }

        if ( isset($_GET['date_consume']) && isset($_GET['totalLts']) && isset($_GET['totalCost']) && isset($_GET['kms']) )
        {
            $date_consume = $_GET['date_consume'];
            $totalLts = $_GET['totalLts'];
            $totalCost = $_GET['totalCost'];
            $kms = $_GET['kms'];
            $QUERY_TABLE = 'queryAddFuelConsume';

            $PARAMETERS = array( $date_consume, $totalCost, $totalLts, $kms );
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_fuel_consume', 'addFuelConsumeJson', $PARAMETERS );
        }
    }

    function addFuelConsumeJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {

        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Fuel consumed General Loaded Successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'ID_UNTIL_REGISTRY_CONSUMED' => $QUERY_DATA_BASE,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        }
    }

    public function getFuelConsumedGeneral()
    {
        $COMPANY = company::UTE;
        $QUERY_TABLE = 'queryFuelConsumedGeneralSearch';
        if ( isset( $_GET['fuelConsumedGeneral_id'] ) )
        {
            $fuelConsumedGeneral_id = $_GET['fuelConsumedGeneral_id'];
            $PARAMETERS = array( $fuelConsumedGeneral_id );
        }
        else
        {
            $PARAMETERS = array( '' );
        }

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_fuel_consumed', 'getFuelConsumedGeneralJson', $PARAMETERS );
    }

    function getFuelConsumedGeneralJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $fuelConsumedGeneral = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'ID_FUEL_CONSUMED_GENERAL' => $fuelConsumedGeneral['ID'],
                    'FUEL_CONSUMED_DATA' => array(
                        'DATE' => $fuelConsumedGeneral['FECHA'],
                        'TOTAL_LTS' => $fuelConsumedGeneral['TOTAL_LTS'],
                        'COMPANY_ID' => $fuelConsumedGeneral['empresa_id'],
                        'COMPANY' => setCompanyDetail( $fuelConsumedGeneral['empresa_id'] )
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }


    public function deleteConsumedGeneral()
    {
        $COMPANY = company::UTE;
        $QUERY_TABLE = 'queryFuelConsumedGeneralDelete';

        if (empty($_GET['fuelConsumedGeneral_id']))
        {
            isRequired( 'fuelConsumedGeneral_id' );
        }
        if ( isset($_GET['fuelConsumedGeneral_id']) )
        {
            $fuelConsumedGeneral_id = $_GET['fuelConsumedGeneral_id'];
            $PARAMETERS = array( $fuelConsumedGeneral_id );
        }

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_fuel_consumed', 'deleteConsumedGeneralJson', $PARAMETERS );
    }

    function deleteConsumedGeneralJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Fuel consumed General Deleted Successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'ID_UNTIL_REGISTRY_CONSUMED' => $QUERY_DATA_BASE,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        }
    }
}
?>